#SDK для подключения оплаты через платежную систему IntellectMoney на языке PHP

> **Внимание!** <br>
Данная версия актуальна на *28 июля 2020 года* и не обновляется. <br>
Актуальную версию можно найти по ссылке https://wiki.intellectmoney.ru/pages/viewpage.action?pageId=37224456.

Описание проекта можно найти здесь: https://wiki.intellectmoney.ru/pages/viewpage.action?pageId=37224456