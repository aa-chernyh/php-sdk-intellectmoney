<?php

namespace PaySystem;

require_once("../IntellectMoneyCommon/Status.php");

class GlobalTestValues {

    protected $rightParams = array(
        'eshopId' => 453162,
        'secretKey' => "12345678",
        'expireDate' => 4199,
        'preference' => "bankcard, inner",
        'successUrl' => "https://mail.google.com/mail/u/0/#inbox/FMfcgxwBVDBmmZTWHFTgClBjXMHkSRhW",
        'backUrl' => "https://mail.google.com/mail/u/0/#inbox/FMfcgxwBVDBmmZTWHFTgClBjXMHkSRhW",
        'resultUrl' => "https://mail.google.com/mail/u/0/#inbox/FMfcgxwBVDBmmZTWHFTgClBjXMHkSRhW",
        'testMode' => false,
        'holdMode' => "true",
        'inn' => "7811142520",
        'holdTime' => "118",
        'group' => "any value",
        'tax' => 3,
        'deliveryTax' => "4",
        "paymentSubjectType" => 2,
        "paymentMethodType" => 3,
        'accountId' => "1234567890",
        'formId' => "154",
        'formType' => "PeerToPeer",
        'statusCreated' => "3",
        'statusCancelled' => "awaiting_status_3",
        'statusPaid' => "stat4",
        'statusHolded' => "status",
        'statusPartiallyPaid' => "partially-paid",
        'statusRefunded' => "6",
        'isUseMerchantReceiptEntities' => true,
        'merchantReceiptType' => 1,
        'merchantURL' => 'https://merchant-test.development.dev.intellectmoney.ru',
        'userFields' => array(
            'UserFieldName_1' => 'Name of param 1',
            'UserField_1' => 'Param 1',
            'UserFieldName_2' => 'Name of param 2',
            'UserField_2' => 'Param 2'
        )
    );
    protected $wrongParams = array(
        'eshopId' => 4374027,
        'secretKey' => "",
        'expireDate' => 4400,
        'preference' => "bankcard  inner,   ,",
        'successUrl' => "www.www.com",
        'backUrl' => "www.www.com",
        'resultUrl' => "www.www.com",
        'testMode' => "0jsdf",
        'holdMode' => "0jsdf",
        'inn' => "123hfj7890",
        'holdTime' => "120",
        'tax' => 7,
        'deliveryTax' => "a",
        "paymentSubjectType" => 0,
        "paymentMethodType" => "aoeuaoeu",
        'accountId' => "123asd7890",
        'formId' => "4a",
        'formType' => "Pear2Pear",
        'statusCreated' => "",
        'statusCancelled' => "",
        'statusPaid' => "",
        'statusHolded' => "",
        'statusPartiallyPaid' => "",
        'statusRefunded' => "",
        'isUseMerchantReceiptEntities' => 7812386,
        'merchantReceiptType' => 4352126484,
        'merchantURL' => 'merchant-test.dev.intellectmoney.ru/ru/?invoiceId=3333333333',
        'userFields' => array(
            'bred_1' => 'Name of param 1',
            'sdfsdfsdfsdf' => 'Param 1'
        )
    );
    protected $halfOfRightParams = array(
        'eshopId' => 458390,
        'secretKey' => "hsdfg`120%^&#$%",
        'expireDate' => 4199,
        'preference' => "bankcard, inner",
        'successUrl' => "https://mail.google.com/mail/u/0/#inbox/FMfcgxwBVDBmmZTWHFTgClBjXMHkSRhW",
        'backUrl' => "https://mail.google.com/mail/u/0/#inbox/FMfcgxwBVDBmmZTWHFTgClBjXMHkSRhW",
        'resultUrl' => "https://mail.google.com/mail/u/0/#inbox/FMfcgxwBVDBmmZTWHFTgClBjXMHkSRhW",
        'testMode' => false,
        'holdMode' => "false",
        'inn' => "1234567890",
        "paymentSubjectType" => 2,
        "paymentMethodType" => 3,
        'holdTime' => "118",
        'group' => "any value",
    );
    protected $halfOfWrongParams = array(
        'tax' => 7,
        'deliveryTax' => "a",
        'accountId' => "123asd7890",
        'formId' => "4a",
        'formType' => "Pear2Pear",
        'statusCreated' => "",
        'statusCancelled' => "",
        'statusPaid' => "",
        'statusHolded' => "",
        'statusPartiallyPaid' => "",
        'statusRefunded' => "",
    );
    protected $paramsContainingDefaultValue = array(
        "expireDate",
        "testMode",
        "holdMode",
        "holdTime",
        "merchantURL",
        "isUseMerchantReceiptEntities",
        "merchantReceiptType",
    );
    protected $customerParams = array(
        'email' => 'a.chernyh+10@intellectmoney.ru',
        'name' => 'Александр Черных',
        'phone' => 89999999999,
    );
    protected $orderParams = array(
        'invoiceId' => 3847563840,
        'orderId' => 12345678,
        'originalAmount' => 300,
        'recipientAmount' => 300,
        'paidAmount' => 50,
        'deliveryAmount' => 100,
        'recipientCurrency' => 'RUB',
        'discount' => 0,
        'status' => Status::created,
    );
    protected $orderParamsWithDifferentValuesAtRecipientAmountAndOriginalAmountWithDiscount = array(
        'invoiceId' => 3847563840,
        'orderId' => 123456,
        'originalAmount' => 325,
        'recipientAmount' => 250,
        'paidAmount' => 50,
        'deliveryAmount' => 100,
        'recipientCurrency' => 'RUB',
        'discount' => 25,
        'status' => Status::created,
    );
    protected $products = array(
        array(
            "price" => 100.00,
            "quantity" => 1,
            "name" => "tovar 1"
        ),
        array(
            "price" => 50.00,
            "quantity" => 1,
            "name" => "tovar 2"
        ),
        array(
            "price" => 50.00,
            "quantity" => 1,
            "name" => "tovar 3"
        ),
    );
    protected $merchantReceipt = '{"inn":"7811142520","group":"any value","content":{"type":1,"positions":[{"price":"100.00","quantity":1,"text":"tovar 1","tax":1,"paymentSubjectType":2,"paymentMethodType":3},{"price":"50.00","quantity":1,"text":"tovar 2","tax":1,"paymentSubjectType":2,"paymentMethodType":3},{"price":"50.00","quantity":1,"text":"tovar 3","tax":1,"paymentSubjectType":2,"paymentMethodType":3},{"price":"100.00","quantity":1,"text":"Delivery","tax":2,"paymentSubjectType":2,"paymentMethodType":3}],"customerContact":89999999999}}';
    protected $merchantReceiptWithDifferentValuesAtRecipientAmountAndOriginalAmountWithDiscount = '{"inn":"7811142520","group":"any value","content":{"type":1,"positions":[{"price":"107.15","quantity":1,"text":"tovar 1","tax":1,"paymentSubjectType":2,"paymentMethodType":3},{"price":35.71,"quantity":1,"text":"tovar 2","tax":1,"paymentSubjectType":2,"paymentMethodType":3},{"price":35.71,"quantity":1,"text":"tovar 3","tax":1,"paymentSubjectType":2,"paymentMethodType":3},{"price":71.43,"quantity":1,"text":"Delivery","tax":2,"paymentSubjectType":2,"paymentMethodType":3}],"customerContact":"a.chernyh+10@intellectmoney.ru"}}';
    protected $ruPaymentParamsForOrganizations = Array(
        'invoiceId' => 3847563840,
        'preference' => 'bankcard',
        'merchantReceipt' => '{"inn":"7811142520","group":"any value","content":{"type":1,"positions":[{"price":"100.00","quantity":1,"text":"tovar 1","tax":3,"paymentSubjectType":2,"paymentMethodType":3},{"price":"50.00","quantity":1,"text":"tovar 2","tax":3,"paymentSubjectType":2,"paymentMethodType":3},{"price":"50.00","quantity":1,"text":"tovar 3","tax":3,"paymentSubjectType":2,"paymentMethodType":3},{"price":"100.00","quantity":1,"text":"Delivery","tax":"4","paymentSubjectType":2,"paymentMethodType":3}],"customerContact":"a.chernyh+10@intellectmoney.ru"}}',
        'eshopId' => 453162,
        'orderId' => 12345678,
        'serviceName' => 'Оплата заказа №12345678',
        'recipientAmount' => "300.00",
        'recipientCurrency' => 'RUB',
        'userEmail' => 'a.chernyh+10@intellectmoney.ru',
        'resultUrl' => 'https://mail.google.com/mail/u/0/#inbox/FMfcgxwBVDBmmZTWHFTgClBjXMHkSRhW',
        'successUrl' => 'https://mail.google.com/mail/u/0/#inbox/FMfcgxwBVDBmmZTWHFTgClBjXMHkSRhW',
        'backUrl' => 'https://mail.google.com/mail/u/0/#inbox/FMfcgxwBVDBmmZTWHFTgClBjXMHkSRhW',
        'holdTime' => 118,
        'holdMode' => 1,
        'hash' => '03621fe0e561fe12d1b3f8d33fe03e45',
    );
    protected $enPaymentParamsForOrganizations = Array(
        'invoiceId' => 3847563840,
        'preference' => 'bankcard',
        'merchantReceipt' => '{"inn":"7811142520","group":"any value","content":{"type":1,"positions":[{"price":"100.00","quantity":1,"text":"tovar 1","tax":3,"paymentSubjectType":2,"paymentMethodType":3},{"price":"50.00","quantity":1,"text":"tovar 2","tax":3,"paymentSubjectType":2,"paymentMethodType":3},{"price":"50.00","quantity":1,"text":"tovar 3","tax":3,"paymentSubjectType":2,"paymentMethodType":3},{"price":"100.00","quantity":1,"text":"Delivery","tax":"4","paymentSubjectType":2,"paymentMethodType":3}],"customerContact":"a.chernyh+10@intellectmoney.ru"}}',
        'eshopId' => 453162,
        'orderId' => 12345678,
        'serviceName' => 'Payment order #12345678',
        'recipientAmount' => "300.00",
        'recipientCurrency' => 'RUB',
        'userEmail' => 'a.chernyh+10@intellectmoney.ru',
        'resultUrl' => 'https://mail.google.com/mail/u/0/#inbox/FMfcgxwBVDBmmZTWHFTgClBjXMHkSRhW',
        'successUrl' => 'https://mail.google.com/mail/u/0/#inbox/FMfcgxwBVDBmmZTWHFTgClBjXMHkSRhW',
        'backUrl' => 'https://mail.google.com/mail/u/0/#inbox/FMfcgxwBVDBmmZTWHFTgClBjXMHkSRhW',
        'holdTime' => 118,
        'hash' => 'e19b1751c362fc39d5cd5b71f3cf30a6',
    );
    protected $ruPaymentParamsForP2P = Array(
        'invoiceId' => 3847563840,
        'UserField_0' => 1234567890,
        'UserFieldName_0' => 'Перевод в кошелек',
        'UserField_9' => 154,
        'UserFieldName_9' => 'UserPaymentFormId',
        'preference' => 'bankcard',
        'eshopId' => 450040,
        'orderId' => 12345678,
        'serviceName' => 'Оплата заказа №12345678',
        'recipientAmount' => "300.00",
        'recipientCurrency' => 'RUB',
        'userEmail' => 'a.chernyh+10@intellectmoney.ru',
        'resultUrl' => 'https://mail.google.com/mail/u/0/#inbox/FMfcgxwBVDBmmZTWHFTgClBjXMHkSRhW',
        'successUrl' => 'https://mail.google.com/mail/u/0/#inbox/FMfcgxwBVDBmmZTWHFTgClBjXMHkSRhW',
        'backUrl' => 'https://mail.google.com/mail/u/0/#inbox/FMfcgxwBVDBmmZTWHFTgClBjXMHkSRhW',
    );
    protected $enPaymentParamsForP2P = Array(
        'invoiceId' => 3847563840,
        'UserField_0' => 1234567890,
        'UserFieldName_0' => 'Перевод в кошелек',
        'UserField_9' => 154,
        'UserFieldName_9' => 'UserPaymentFormId',
        'preference' => 'bankcard',
        'eshopId' => 450040,
        'orderId' => 12345678,
        'serviceName' => 'Payment order #12345678',
        'recipientAmount' => "300.00",
        'recipientCurrency' => 'RUB',
        'userEmail' => 'a.chernyh+10@intellectmoney.ru',
        'resultUrl' => 'https://mail.google.com/mail/u/0/#inbox/FMfcgxwBVDBmmZTWHFTgClBjXMHkSRhW',
        'successUrl' => 'https://mail.google.com/mail/u/0/#inbox/FMfcgxwBVDBmmZTWHFTgClBjXMHkSRhW',
        'backUrl' => 'https://mail.google.com/mail/u/0/#inbox/FMfcgxwBVDBmmZTWHFTgClBjXMHkSRhW',
    );
    protected $requestParamsForSetStatus = array(
        'eshopId' => 453185,
        'paymentId' => 3847563840,
        'orderId' => 12345678,
        'eshopAccount' => 4881817598,
        'serviceName' => '%d0%9e%d0%bf%d0%bb%d0%b0%d1%82%d0%b0+%d1%81%d0%b5%d1%80%d1%82%d0%b8%d1%84%d0%b8%d0%ba%d0%b0%d1%82%d0%b0+%2321554846',
        'recipientAmount' => '300.00',
        'recipientOriginalAmount' => '300.00',
        'recipientCurrency' => 'RUB',
        'secretKey' => "12345678",
        'userEmail' => 'a.chernyh+2000@intellectmoney.ru',
        'paymentData' => '2019-02-13 12:02:19',
        'userName' => "Alex Black",
    );
    protected $userSettingsForSetStatusTest = array(
        'eshopId' => 453185,
        'secretKey' => "12345678",
        'inn' => "7811142520",
        'tax' => 3,
        'deliveryTax' => "4",
    );
    protected $orderParamsForSetStatusTest = array(
        'invoiceId' => 3847563840,
        'orderId' => 12345678,
        'originalAmount' => 300,
        'recipientAmount' => 300,
        'paidAmount' => 0,
        'deliveryAmount' => 100,
        'recipientCurrency' => 'RUB',
        'discount' => 0
    );

    protected function showResult($result, $functionName, $errorText = "") {
        echo $functionName, ": <b>";
        echo $result ? "OK" : "FAIL", "</b>";
        if (!empty($errorText)) {
            echo " ", $errorText;
        }
        $this->marginBottom();
    }

    protected function getDiffs($arr1, $arr2) {
        return array_merge(array_diff_assoc($arr1, $arr2), array_diff_assoc($arr2, $arr1));
    }

    protected function generateErrorText($errorParams, $objects = 'functions') {
        $errorText = "";
        if (!empty($errorParams)) {
            $errorText = "Error " . $objects . ": ";
            foreach ($errorParams as $value) {
                $errorText .= $value . " ";
            }
        }
        return $errorText;
    }

    protected function getErrorFunctions($class, $forParams) {
        $errorFunctions = array();
        foreach ($forParams as $key => $value) {
            $functionName = "get" . ucfirst($key);
            $functionResult = $class->$functionName();
            if ($value != $functionResult) {
                $errorFunctions[] = $key;
            }
        }
        return $errorFunctions;
    }

    protected function resetAndSetAllParams($class, $params) {
        $class->resetParams();
        foreach ($params as $key => $value) {
            $functionName = "set" . ucfirst($key);
            $class->$functionName($value);
        }
    }

    private function marginBottom() {
        echo "<br>";
    }

    protected function convertToPriceFormat($amount) {
        return number_format($amount, 2, '.', '');
    }

}
