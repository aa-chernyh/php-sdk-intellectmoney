<?php

namespace PaySystem;

require_once("GlobalTestValues.php");
require_once("../IntellectMoneyCommon/UserSettings.php");

class UserSettingsTest extends GlobalTestValues {

    private static $instance;
    private $UserSettings;

    public static function getInstance($isAllowChangeValue = false) {
        if (empty(self::$instance)) {
            self::$instance = new self($isAllowChangeValue);
        }
        return self::$instance;
    }

    private function __construct($isAllowChangeValue) {
        $this->UserSettings = UserSettings::getInstance(array(), $isAllowChangeValue);
    }

    public function start() {
        echo "<b>Common tests:</b> <br>";
        $this->setIsAllowChangeValuesTest();
        $this->getConvertedExpireDateTest();
        $this->resetParamsTest();
        $this->setAndGetMerchantReceiptEntitiesTest();

        echo "<br><b>Allowed change values:</b> <br>";
        $this->UserSettings->setIsAllowChangeValues(true);
        $this->setRightParamsTest();
        $this->setWrongParamsTest();
        $this->setMixedParamsTest();
        $this->getParamsTest();
        $this->getAllRightParamsTest();
        $this->getAllWrongParamsTest();
        $this->setAllRightParamsTest();
        $this->setAllWrongParamsWithAllowChangeValuesTest();

        echo "<br><b>Disallowed change values:</b> <br>";
        $this->UserSettings->setIsAllowChangeValues(false);
        $this->setRightParamsTest();
        $this->setWrongParamsTest();
        $this->setMixedParamsTest();
        $this->getParamsTest();
        $this->getAllRightParamsTest();
        $this->getAllWrongParamsTest();
        $this->setAllRightParamsTest();
        $this->setAllWrongParamsWithoutAllowChangeValuesTest();
    }

    public function setRightParamsTest() {
        $this->UserSettings->resetParams();

        $errors = array();
        $this->UserSettings->setParams($this->rightParams, $errors);

        $this->showResult(empty($errors), __FUNCTION__);
    }

    public function setWrongParamsTest() {
        $errors = $this->setParamsAndGetErrors($this->wrongParams);
        $diffs = $this->getDiffs($errors, $this->wrongParams);

        $this->showResult(empty($this->getDiffsWithoutDefaultValues($diffs)), __FUNCTION__);
    }

    public function setMixedParamsTest() {
        $mixedParams = array_merge($this->halfOfRightParams, $this->halfOfWrongParams);
        $errors = $this->setParamsAndGetErrors($mixedParams);
        $diffs = $this->getDiffs($errors, $this->halfOfWrongParams);

        $this->showResult(empty($this->getDiffsWithoutDefaultValues($diffs)), __FUNCTION__);
    }

    public function getParamsTest() {
        $sendParams = $this->rightParams;
        $this->setParamsAndGetErrors($sendParams);
        $savedParams = $this->UserSettings->getParams();
        if ($sendParams['formType'] == 'PeerToPeer') {
            $sendParams['preference'] = 'bankcard';
        }
        $diffs = $this->getDiffs($sendParams, $savedParams);
        unset($diffs['defaultEshopIdForP2P']);

        $this->showResult(empty($diffs), __FUNCTION__);
    }

    public function setIsAllowChangeValuesTest() {
        $startValueOfIsAllowChangeValue = $this->UserSettings->getIsAllowChangeValues();

        $this->UserSettings->setIsAllowChangeValues(true);
        $trueIsAllowChangeValue = $this->UserSettings->getIsAllowChangeValues();
        $this->UserSettings->setIsAllowChangeValues(false);
        $falseIsAllowChangeValue = $this->UserSettings->getIsAllowChangeValues();
        $result = false;
        if ($trueIsAllowChangeValue != $falseIsAllowChangeValue && $trueIsAllowChangeValue === true && $falseIsAllowChangeValue === false) {
            $result = true;
        }

        $this->UserSettings->setIsAllowChangeValues($startValueOfIsAllowChangeValue);
        $this->showResult($result, __FUNCTION__);
    }

    public function getConvertedExpireDateTest() {
        $this->UserSettings->setExpireDate(10);
        $calculatedDate = date('Y-m-d H:i:s', strtotime('+' . 10 . ' hour'));
        $this->showResult($calculatedDate == $this->UserSettings->getConvertedExpireDate(), __FUNCTION__);
    }

    public function getAllRightParamsTest() {
        $this->setParamsAndGetErrors($this->rightParams);

        $errors = array();
        foreach ($this->rightParams as $key => $value) {
            $functionName = "get" . ucfirst($key);
            $functionResult = $this->UserSettings->$functionName();
            if (!isset($functionResult)) {
                $errors[] = $functionName;
            }
        }

        if (!empty($errors)) {
            $errorText = "Error functions: ";
        }
        foreach ($errors as $value) {
            $errorText .= $value . "() ";
        }
        $this->showResult(empty($errors), __FUNCTION__, $errorText);
    }

    public function getAllWrongParamsTest() {
        $this->setParamsAndGetErrors($this->wrongParams);

        $result = true;
        foreach ($this->wrongParams as $key => $value) {
            $functionName = "get" . ucfirst($key);

            if (!(empty($this->UserSettings->$functionName()) || !$this->UserSettings->getIsAllowChangeValues() || in_array($key, $this->paramsContainingDefaultValue))) {
                $result = false;
            }
        }

        $this->showResult($result, __FUNCTION__);
    }

    public function setAllRightParamsTest() {
        $this->setParamsAndGetErrors($this->rightParams);

        $errors = array();
        foreach ($this->rightParams as $key => $value) {
            $functionName = "set" . ucfirst($key);
            if (!$this->UserSettings->$functionName($this->rightParams[$key])) {
                $errors[] = $functionName;
            }
        }

        if (!empty($errors)) {
            $errorText = "Error functions: ";
            foreach ($errors as $value) {
                $errorText .= $value . "() ";
            }
        }
        $this->showResult(empty($errors), __FUNCTION__, $errorText);
    }

    public function setAllWrongParamsWithAllowChangeValuesTest() {
        $this->setParamsAndGetErrors($this->wrongParams);

        $result = true;
        foreach ($this->getResultOfSetParams($this->wrongParams) as $value) {
            if ($value === true && !in_array($value, $this->paramsContainingDefaultValue)) {
                $result = false;
            }
        }

        $this->showResult($result, __FUNCTION__);
    }

    public function setAllWrongParamsWithoutAllowChangeValuesTest() {
        $this->setParamsAndGetErrors($this->wrongParams);

        $result = true;
        foreach ($this->getResultOfSetParams($this->wrongParams) as $value) {
            if ($value === true) {
                $result = false;
            }
        }

        $this->showResult($result, __FUNCTION__);
    }

    public function resetParamsTest() {
        $this->UserSettings->setParams($this->wrongParams);
        $this->UserSettings->resetParams();

        $result = true;
        foreach ($this->UserSettings->getParams() as $value) {
            if (!empty($value)) {
                $result = false;
            }
        }

        $this->showResult($result, __FUNCTION__);
    }

    public function setAndGetMerchantReceiptEntitiesTest() {
        $originalValueofEntities = $this->UserSettings->getIsUseMerchantReceiptEntities();

        $this->UserSettings->setIsUseMerchantReceiptEntities(true);
        $getTrue = $this->UserSettings->getIsUseMerchantReceiptEntities();

        $this->UserSettings->setIsUseMerchantReceiptEntities(false);
        $getFalse = $this->UserSettings->getIsUseMerchantReceiptEntities();

        $result = false;
        if ($getTrue != $getFalse && $getTrue === true && $getFalse === false) {
            $result = true;
        }
        $this->showResult($result, __FUNCTION__);

        $this->UserSettings->setIsUseMerchantReceiptEntities($originalValueofEntities);
    }

    private function getDiffsWithoutDefaultValues($diffs) {
        if ($this->UserSettings->getIsAllowChangeValues()) {
            foreach ($this->paramsContainingDefaultValue as $value) {
                unset($diffs[$value]);
            }
        }
        return $diffs;
    }

    private function setParamsAndGetErrors($params) {
        $this->UserSettings->resetParams();
        $errors = array();
        $this->UserSettings->setParams($params, $errors);
        return $errors;
    }

    private function getResultOfSetParams($params) {
        $resultOfsetParams = array();
        foreach ($params as $key => $value) {
            $functionName = "set" . ucfirst($key);
            $resultOfsetParams[$functionName] = $this->UserSettings->$functionName($this->wrongParams[$key]);
        }
        return $resultOfsetParams;
    }

}

$ust = UserSettingsTest::getInstance();
$ust->start();
?>

