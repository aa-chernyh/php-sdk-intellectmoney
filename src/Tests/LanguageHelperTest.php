<?php

namespace PaySystem;

require_once("../IntellectMoneyCommon/LanguageHelper.php");
require_once("GlobalTestValues.php");

class LanguageHelperTest extends GlobalTestValues {

    private static $instance;
    private $LanguageHelper;

    public static function getInstance() {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct() {
        $this->LanguageHelper = LanguageHelper::getInstance();
    }

    public function start() {
        $this->changeLangTest('en');
        $this->changeLangTest('ru');
        $this->changeLangWithEncodingTest('en');
        $this->changeLangWithEncodingTest('ru');
        $this->changeLangToRuWithDecodingTest(true);
        $this->getTitleTest();
        $this->getDescTest();
        $this->getErrorTest();
        $this->setUndefinedLangTest();
        $this->getUndefinedTitleTest();
        $this->getUndefinedDescTest();
        $this->getUndefinedErrorTest();
    }

    private function setLangTest($newLang) {
        if (strtolower($newLang) == $this->LanguageHelper->getLanguage()) {
            return true;
        }
        return false;
    }

    private function setEncodingTest($strInOldEncoding) {
        $oldEncoding = strtolower(mb_detect_encoding($strInOldEncoding, "UTF-8, cp1251"));
        $newEncoding = strtolower(mb_detect_encoding($this->LanguageHelper->getTitle('eshopId'), "UTF-8, cp1251"));

        if ($this->LanguageHelper->getLanguage() == 'ru') {
            return $newEncoding != $oldEncoding;
        } else {
            return $newEncoding == $oldEncoding;
        }
    }

    public function changeLangTest($lang) {
        $this->LanguageHelper->setLanguage($lang);
        $result = $this->setLangTest($lang);

        $this->showResult($result, __FUNCTION__ . " Lang: " . $lang);
    }

    public function changeLangWithEncodingTest($lang) {
        $strInOldEncoding = $this->LanguageHelper->getTitle('eshopId');
        $this->LanguageHelper->setLanguage($lang, true);
        $langResult = $this->setLangTest($lang);
        $encodingResult = $this->setEncodingTest($strInOldEncoding);
        $result = $langResult && $encodingResult;

        $this->showResult($result, __FUNCTION__ . " Lang: " . $lang);
    }

    public function changeLangToRuWithDecodingTest() {
        $strInOldEncoding = $this->LanguageHelper->getTitle('eshopId');

        $this->LanguageHelper->setLanguage('ru', false);
        $langResult = $this->setLangTest('ru');
        $encodingResult = $this->setEncodingTest($strInOldEncoding);
        $result = $langResult && $encodingResult;

        $this->showResult($result, __FUNCTION__);
    }

    public function getTitleTest() {
        $this->showResult($this->LanguageHelper->getTitle('tax'), __FUNCTION__);
    }

    public function getDescTest() {
        $this->showResult($this->LanguageHelper->getDesc('tax'), __FUNCTION__);
    }

    public function getErrorTest() {
        $this->showResult($this->LanguageHelper->getError('orderNotFound'), __FUNCTION__);
    }

    public function setUndefinedLangTest() {
        $this->showResult($this->undefinedObjectProcessor('lang'), __FUNCTION__);
    }

    public function getUndefinedTitleTest() {
        $this->showResult($this->undefinedObjectProcessor('title'), __FUNCTION__);
    }

    public function getUndefinedDescTest() {
        $this->showResult($this->undefinedObjectProcessor('desc'), __FUNCTION__);
    }

    public function getUndefinedErrorTest() {
        $this->showResult($this->undefinedObjectProcessor('error'), __FUNCTION__);
    }

    private function undefinedObjectProcessor($object) {
        $result = false;
        try {
            switch ($object) {
                case 'lang': $this->LanguageHelper->setLanguage('bred');
                    break;
                case 'title': $this->LanguageHelper->getTitle('bred');
                    break;
                case 'desc': $this->LanguageHelper->getDesc('bred');
                    break;
                case 'error': $this->LanguageHelper->getError('bred');
                    break;
                default: break;
            }
        } catch (Exceptions\LanguageHelperException $e) {
            $result = true;
        } finally {
            return $result;
        }
    }

}

$LanguageHelperTest = LanguageHelperTest::getInstance();
$LanguageHelperTest->start();
?>

