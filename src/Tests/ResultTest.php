<?php

namespace PaySystem;

error_reporting(E_ERROR);
ini_set("display_errors", 1);

require_once("GlobalTestValues.php");
require_once("../IntellectMoneyCommon/UserSettings.php");
require_once("../IntellectMoneyCommon/Order.php");
require_once("../IntellectMoneyCommon/LanguageHelper.php");
require_once("../IntellectMoneyCommon/Result.php");
require_once("../IntellectMoneyCommon/StatusWeight.php");

class ResultTest extends GlobalTestValues {

    private static $instance;
    private $UserSettings;
    private $Order;
    private $LanguageHelper;
    private $Result;

    public static function getInstance() {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct() {
        $this->UserSettings = UserSettings::getInstance();
        $this->Order = Order::getInstance();
        $this->LanguageHelper = LanguageHelper::getInstance('ru');
        $this->Result = Result::getInstance();
        $_SERVER['REMOTE_ADDR'] = "127.0.0.1";
    }

    private function setParamsToResult($request, $UserSettingsParams, $OrderParams, $lang = 'ru') {
        $this->LanguageHelper->setLanguage($lang);
        $this->UserSettings->setParams($UserSettingsParams);
        $this->Order->setParams($OrderParams['invoiceId'], $OrderParams['orderId'], $OrderParams['originalAmount'], $OrderParams['recipientAmount'], $OrderParams['paidAmount'], $OrderParams['deliveryAmount'], $OrderParams['recipientCurrency'], $OrderParams['discount'], $OrderParams['status']);

        if (!isset($request['hash'])) {
            $requestHashStr = implode('::', array($request['eshopId'], $request['orderId'], $request['serviceName'], $request['eshopAccount'], $request['recipientAmount'], strtoupper($request['recipientCurrency']), $request['paymentStatus'], $request['userName'], $request['userEmail'], $request['paymentData'], $this->UserSettings->getSecretKey()));
            $request['hash'] = md5($requestHashStr);
        }
        $this->Result->setParams($request, $this->UserSettings, $this->Order);
    }

    public function start() {
        $this->setCreatedStatusTest();
        $this->setCancelledStatusTest();
        $this->setPaidStatusTest();
        $this->setHoldedStatusTest();
        $this->setPartiallyPaidStatusTest();
        $this->setRefundedStatusTest();
        $this->setUndefinedStatus();
        $this->checkAmountTest();
        $this->checkCurrencyTest('RUB');
        $this->checkCurrencyTest('RUR');
        $this->checkCurrencyTest('TST');
        $this->checkEshopIdTest();
        $this->checkSecretKeyTest();
        $this->checkWrongHashTest();
        $this->checkTrueHashTest();
        $this->checkWrongIPTest();
        $this->checkTrueIPTest();
    }

    public function setCreatedStatusTest() {
        $this->requestParamsForSetStatus['paymentStatus'] = Status::created;
        $this->showResult($this->setStatusTest($this->requestParamsForSetStatus, $this->userSettingsForSetStatusTest, $this->orderParamsForSetStatusTest), __FUNCTION__);
    }

    private function setStatusTest($request, $userSettings, $order) {
        $this->Result->resetParams();

        $weights = StatusWeight::getWeightArray();
        $statusWithWeightOneLessThanStatusFromRequest = array_search($weights[$request['paymentStatus']] - 1, $weights);
        $order['status'] = $statusWithWeightOneLessThanStatusFromRequest;
        $this->setParamsToResult($request, $userSettings, $order);

        $changeStatusResult = $this->Result->processingResponse()->changeStatusResult;
        $result = $this->Result->getMessage() == 'OK' ? true : false;
        return $result && $changeStatusResult;
    }

    public function setCancelledStatusTest() {
        $this->requestParamsForSetStatus['paymentStatus'] = Status::cancelled;
        $this->showResult($this->setStatusTest($this->requestParamsForSetStatus, $this->userSettingsForSetStatusTest, $this->orderParamsForSetStatusTest), __FUNCTION__);
    }

    public function setPaidStatusTest() {
        $this->requestParamsForSetStatus['paymentStatus'] = Status::paid;
        $this->showResult($this->setStatusTest($this->requestParamsForSetStatus, $this->userSettingsForSetStatusTest, $this->orderParamsForSetStatusTest), __FUNCTION__);
    }

    public function setHoldedStatusTest() {
        $this->requestParamsForSetStatus['paymentStatus'] = Status::holded;
        $this->showResult($this->setStatusTest($this->requestParamsForSetStatus, $this->userSettingsForSetStatusTest, $this->orderParamsForSetStatusTest), __FUNCTION__);
    }

    public function setPartiallyPaidStatusTest() {
        $this->requestParamsForSetStatus['paymentStatus'] = Status::partiallyPaid;
        $tempRequestParams = array(
            'recipientAmount' => "100.00",
        );
        $this->showResult($this->setStatusTest(array_merge($this->requestParamsForSetStatus, $tempRequestParams), $this->userSettingsForSetStatusTest, $this->orderParamsForSetStatusTest), __FUNCTION__);
    }

    public function setRefundedStatusTest() {
        $this->requestParamsForSetStatus['paymentStatus'] = Status::refunded;
        $this->showResult($this->setStatusTest($this->requestParamsForSetStatus, $this->userSettingsForSetStatusTest, $this->orderParamsForSetStatusTest), __FUNCTION__);
    }

    public function setUndefinedStatus() {
        $this->requestParamsForSetStatus['paymentStatus'] = Status::undefined;
        $this->showResult($this->setStatusTest($this->requestParamsForSetStatus, $this->userSettingsForSetStatusTest, $this->orderParamsForSetStatusTest), __FUNCTION__);
    }

    public function checkAmountTest() {
        $this->requestParamsForSetStatus['paymentStatus'] = Status::partiallyPaid;
        $isSetStatus = $this->setStatusTest($this->requestParamsForSetStatus, $this->userSettingsForSetStatusTest, $this->orderParamsForSetStatusTest);
        $this->showResult(!$isSetStatus && $this->Result->getMessage() == $this->LanguageHelper->getError('amountDidNotMatch'), __FUNCTION__);
    }

    public function checkCurrencyTest($currency) {
        $currency = strtoupper($currency);

        $this->Order->setRecipientCurrency($currency);
        $this->UserSettings->setTestMode($this->Order->getRecipientCurrency() == 'TST');
        $tempRequestParams = array(
            'recipientCurrency' => "BRED",
            'paymentStatus' => Status::created,
        );
        $isSetStatus = $this->setStatusTest(array_merge($this->requestParamsForSetStatus, $tempRequestParams), $this->userSettingsForSetStatusTest, $this->orderParamsForSetStatusTest);

        $this->showResult(!$isSetStatus && $this->Result->getMessage() == $this->LanguageHelper->getError('currencyDidNotMatch'), __FUNCTION__ . ". Currency: " . $currency);
    }

    public function checkEshopIdTest() {
        $tempParamsForRequest = array(
            'eshopId' => 451111,
            'paymentStatus' => Status::created,
        );
        $isSetStatus = $this->setStatusTest(array_merge($this->requestParamsForSetStatus, $tempParamsForRequest), $this->userSettingsForSetStatusTest, $this->orderParamsForSetStatusTest);
        $this->showResult(!$isSetStatus && $this->Result->getMessage() == $this->LanguageHelper->getError('eshopIdDidNotMatch'), __FUNCTION__);
    }

    private function checkHashTest($hash) {
        $tempParamsForRequest = array(
            'paymentStatus' => Status::created,
            'hash' => $hash,
        );
        $this->UserSettings->setEshopId(450111);
        $isSetStatus = $this->setStatusTest(array_merge($this->requestParamsForSetStatus, $tempParamsForRequest), $this->userSettingsForSetStatusTest, $this->orderParamsForSetStatusTest);
        return $isSetStatus;
    }

    public function checkWrongHashTest() {
        $this->showResult(!$this->checkHashTest("f5bb0c8de146c67b44babbf4e6584cc0") && preg_match("/(Хеш)|(Hash)/u", $this->Result->getMessage()), __FUNCTION__);
    }

    public function checkSecretKeyTest() {
        $tempParamsForRequest = array(
            'secretKey' => "bred",
            'paymentStatus' => Status::created,
        );
        $isSetStatus = $this->setStatusTest(array_merge($this->requestParamsForSetStatus, $tempParamsForRequest), $this->userSettingsForSetStatusTest, $this->orderParamsForSetStatusTest);
        $this->showResult(!$isSetStatus && $this->Result->getMessage() == $this->LanguageHelper->getError('secretKeyDidNotMatch'), __FUNCTION__);
    }

    public function checkTrueHashTest() {
        $this->showResult($this->checkHashTest("39c69ae9e131ef980be3fc4abe87d352") && $this->Result->getMessage() == "OK", __FUNCTION__);
    }

    public function checkWrongIPTest() {
        $this->showResult(!$this->checkIPTest("127.10.15.24") && $this->Result->getMessage() == "", __FUNCTION__);
    }

    private function checkIPTest($IP) {
        $tempParamsForRequest = array(
            'paymentStatus' => Status::created,
            'secretKey' => "bred",
        );
        $_SERVER['REMOTE_ADDR'] = $IP;
        return $this->setStatusTest(array_merge($this->requestParamsForSetStatus, $tempParamsForRequest), $this->userSettingsForSetStatusTest, $this->orderParamsForSetStatusTest);
    }

    public function checkTrueIPTest() {
        $this->showResult(!$this->checkIPTest("194.147.107.254") && $this->Result->getMessage() == $this->LanguageHelper->getError('secretKeyDidNotMatch'), __FUNCTION__);
    }

}

$rt = ResultTest::getInstance();
$rt->start();
