<?php

/*
 * Аккаунты, используемые в тестах: 
 * a.chernyh+1000@intellectmoney.ru - физик
 * 12345678 - пароль
 * a.chernyh+2000@intellectmoney.ru - юрик
 * 12345678 - пароль
 * https://jira.im/browse/DOC-5825 - Задача с доступами
 */

namespace PaySystem;

require_once("GlobalTestValues.php");
require_once("../IntellectMoneyCommon/Payment.php");
require_once("../IntellectMoneyCommon/UserSettings.php");
require_once("../IntellectMoneyCommon/Order.php");
require_once("../IntellectMoneyCommon/Customer.php");
require_once("../IntellectMoneyCommon/Item.php");
require_once("../IntellectMoneyCommon/LanguageHelper.php");

class PaymentTest extends GlobalTestValues {

    private static $instance;
    private $UserSettings;
    private $Order;
    private $Customer;
    private $Payment;
    private $LanguageHelper;

    public static function getInstance() {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct() {
        $this->initializeClasses();
        $this->generateItems();
        $this->Payment = Payment::getInstance($this->UserSettings, $this->Order, $this->Customer);
        $this->UserSettings->setIsUseMerchantReceiptEntities(false);
        $this->LanguageHelper = LanguageHelper::getInstance();
    }

    private function initializeClasses() {
        $this->UserSettings = UserSettings::getInstance();
        $this->Order = Order::getInstance();
        $this->Customer = Customer::getInstance();
        $this->LanguageHelper = LanguageHelper::getInstance();
        $this->resetStartParams();
    }

    private function generateItems() {
        foreach ($this->products as $product) {
            $this->Order->addItem($product['price'], $product['quantity'], $product['name'], $this->UserSettings->getTax());
        }
        $this->Order->addItem($this->Order->getDeliveryAmount(), 1, "Delivery", $this->UserSettings->getDeliveryTax(), true);
    }

    public function start() {
        $this->getRuParamsForOrganizationTest();
        $this->getRuParamsForP2PTest();
        $this->sendRuImAccountFormForP2P();
        $this->sendRuDynamicFormForP2P();
        $this->sendRuPeerToPeerFormForP2P();
        $this->sendRuFormForOrganizationWithCashboxAndRUBCurrency();
        $this->sendRuFormForOrganizationWithCashboxAndTSTCurrency();
        $this->sendRuFormForOrganizationWithoutCashboxAndRUBCurrency();
        $this->getEnParamsForOrganizationTest();
        $this->getEnParamsForP2PTest();
        $this->sendEnImAccountFormForP2P();
        $this->sendEnDynamicFormForP2P();
        $this->sendEnPeerToPeerFormForP2P();
        $this->sendEnFormForOrganizationWithCashboxAndRUBCurrency();
        $this->sendEnFormForOrganizationWithCashboxAndTSTCurrency();
        $this->sendEnFormForOrganizationWithoutCashboxAndRUBCurrency();
    }

    public function getRuParamsForOrganizationTest() {
        $this->showResult($this->getParamsForFormTest('ru', true, $this->ruPaymentParamsForOrganizations), __FUNCTION__);
    }

    public function getRuParamsForP2PTest() {
        $this->showResult($this->getParamsForFormTest('ru', false, $this->ruPaymentParamsForP2P), __FUNCTION__);
    }

    public function sendRuImAccountFormForP2P() {
        $this->showResult($this->sendParamsForP2P('ru', 'IMAccount', 85), __FUNCTION__);
    }

    public function sendRuDynamicFormForP2P() {
        $this->showResult($this->sendParamsForP2P('ru', 'Dynamic', 86), __FUNCTION__);
    }

    public function sendRuPeerToPeerFormForP2P() {
        $this->showResult($this->sendParamsForP2P('ru', 'PeerToPeer', 87), __FUNCTION__);
    }

    public function sendRuFormForOrganizationWithCashboxAndRUBCurrency() {
        $this->showResult($this->sendParamsForOrganization('ru', '453185', 'RUB'), __FUNCTION__);
    }

    public function sendRuFormForOrganizationWithCashboxAndTSTCurrency() {
        $this->showResult($this->sendParamsForOrganization('ru', '453186', 'TST'), __FUNCTION__);
    }

    public function sendRuFormForOrganizationWithoutCashboxAndRUBCurrency() {
        $this->showResult($this->sendParamsForOrganization('ru', '453187', 'RUB'), __FUNCTION__);
    }

    public function getEnParamsForOrganizationTest() {
        $this->showResult($this->getParamsForFormTest('en', true, $this->enPaymentParamsForOrganizations), __FUNCTION__);
    }

    public function getEnParamsForP2PTest() {
        $this->showResult($this->getParamsForFormTest('en', false, $this->enPaymentParamsForP2P), __FUNCTION__);
    }

    public function sendEnImAccountFormForP2P() {
        $this->showResult($this->sendParamsForP2P('en', 'IMAccount', 85), __FUNCTION__);
    }

    public function sendEnDynamicFormForP2P() {
        $this->showResult($this->sendParamsForP2P('en', 'Dynamic', 86), __FUNCTION__);
    }

    public function sendEnPeerToPeerFormForP2P() {
        $this->showResult($this->sendParamsForP2P('en', 'PeerToPeer', 87), __FUNCTION__);
    }

    public function sendEnFormForOrganizationWithCashboxAndRUBCurrency() {
        $this->showResult($this->sendParamsForOrganization('en', '453185', 'RUB'), __FUNCTION__);
    }

    public function sendEnFormForOrganizationWithCashboxAndTSTCurrency() {
        $this->showResult($this->sendParamsForOrganization('en', '453186', 'TST'), __FUNCTION__);
    }

    public function sendEnFormForOrganizationWithoutCashboxAndRUBCurrency() {
        $this->showResult($this->sendParamsForOrganization('en', '453187', 'RUB'), __FUNCTION__);
    }

    private function getParamsForFormTest($lang, $isOrganization, $pattern) {
        $this->resetStartParams($lang, $isOrganization);
        $returnParams = $isOrganization ? $this->Payment->getParamsForOrganization() : $this->Payment->getParamsForP2P();

        $pattern['expireDate'] = $this->UserSettings->getConvertedExpireDate(); //Добавляем здесь, потому что expireDate рассчитывается исходя из текущего времени
        
        if (isset($returnParams['invoiceId'])) {
            $pattern = array("invoiceId" => $pattern["invoiceId"]);
        } else {
            unset($pattern['invoiceId']);
        }
        return empty($this->getDiffs($returnParams, $pattern));
    }

    private function resetStartParams($lang = 'ru') {
        $this->UserSettings->setParams($this->rightParams);
        $this->Order->setParams($this->orderParams['invoiceId'], $this->orderParams['orderId'], $this->orderParams['originalAmount'], $this->orderParams['recipientAmount'], $this->orderParams['paidAmount'], $this->orderParams['deliveryAmount'], $this->orderParams['recipientCurrency'], $this->orderParams['discount'], $this->orderParams['status']);
        $this->Customer->setEmail($this->customerParams['email']);
        $this->Customer->setName($this->customerParams['name']);
        $this->Customer->setPhone($this->customerParams['phone']);
        $this->UserSettings->setIsUseMerchantReceiptEntities(false);
        $this->LanguageHelper->setLanguage($lang);
        $this->UserSettings->setDefaultTestEshopIdForP2P();
        $this->UserSettings->setMerchantUrl('https://merchant-test.development.dev.intellectmoney.ru');
    }

    private function sendParamsForP2P($lang, $formType, $formId) {
        $this->resetStartParams($lang);
        $this->UserSettings->setPreference("inner");
        $this->UserSettings->setFormType($formType);
        $this->UserSettings->setAccountId(1930114354);
        $this->UserSettings->setFormId($formId);
        $this->Customer->setEmail('a.chernyh+1000@intellectmoney.ru');
        $this->Order->setOrderId(time());
        $this->UserSettings->setSecretKey(12345678);

        return $this->isSuccessCurlRequest($this->UserSettings->getMerchantUrl(), $this->Payment->getParamsForP2P());
    }

    private function sendParamsForOrganization($lang, $eshopId, $currency) {
        $this->resetStartParams($lang);
        $this->UserSettings->setEshopId($eshopId);
        $this->UserSettings->setSecretKey('12345678');
        $this->Customer->setEmail($currency == 'TST' ? 'a.chernyh+2000@intellectmoney.ru' : 'a.chernyh+1000@intellectmoney.ru');
        $this->Order->setRecipientCurrency($currency);
        $this->Order->setOrderId(time());
        $this->UserSettings->setInn(7812647931);

        return $this->isSuccessCurlRequest($this->UserSettings->getMerchantUrl(), $this->Payment->getParamsForOrganization());
    }

    private function isSuccessCurlRequest($url, $data) {
        $queryString = http_build_query($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $queryString);
        $merchantRequest = curl_exec($ch);
        curl_close($ch);

        preg_match("/Выбор способа оплаты/", $merchantRequest, $res);
        return isset($res[0]);
    }

}

$pt = PaymentTest::getInstance();
$pt->start();
?>