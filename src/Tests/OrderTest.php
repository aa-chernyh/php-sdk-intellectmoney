<?php

namespace PaySystem;

require_once("GlobalTestValues.php");
require_once("../IntellectMoneyCommon/Order.php");
require_once("../IntellectMoneyCommon/Status.php");

error_reporting(E_ERROR);
ini_set("display_errors", 1);

class OrderTest extends GlobalTestValues {

    private static $instance;
    private $Order;

    public static function getInstance() {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct() {
        $this->Order = Order::getInstance();
    }

    public function start() {
        $this->setAndGetParamsTest();
        $this->getAllParamsTest();
        $this->setAllParamsWithoutStatusesTest();
        $this->setWrongRecipientCurrencyTest();
        $this->setStatusTest();
        $this->addAndGetItemsTest();
    }

    public function setAndGetParamsTest() {
        $this->Order->resetParams();
        $this->Order->setParams($this->orderParams['invoiceId'], $this->orderParams['orderId'], $this->orderParams['originalAmount'], $this->orderParams['recipientAmount'], $this->orderParams['paidAmount'], $this->orderParams['deliveryAmount'], $this->orderParams['recipientCurrency'], $this->orderParams['discount'], $this->orderParams['status']);

        $errorParams = array();
        foreach ($this->Order->getParams() as $key => $value) {
            if ($value != $this->orderParams[$key]) {
                $errorParams[] = $key;
            }
        }

        $this->showResult(empty($errorParams), __FUNCTION__, $this->generateErrorText($errorParams));
    }

    public function getAllParamsTest() {
        $this->Order->resetParams();
        $this->Order->setParams($this->orderParams['invoiceId'], $this->orderParams['orderId'], $this->orderParams['originalAmount'], $this->orderParams['recipientAmount'], $this->orderParams['paidAmount'], $this->orderParams['deliveryAmount'], $this->orderParams['recipientCurrency'], $this->orderParams['discount'], $this->orderParams['status']);

        $errorFunctions = $this->getErrorFunctions($this->Order, $this->orderParams);
        $this->showResult(empty($errorFunctions), __FUNCTION__, $this->generateErrorText($errorFunctions));
    }

    public function setAllParamsWithoutStatusesTest() {
        $orderParams = $this->orderParams;
        $this->resetAndSetAllParams($this->Order, $orderParams);

        $recievedParams = $this->Order->getParams();
        $diffs = $this->getDiffs($this->orderParams, $recievedParams);
        $diffsAfterCostCompare = $this->compareCost($diffs, $recievedParams);

        $this->showResult(empty($diffsAfterCostCompare), __FUNCTION__, $this->generateErrorText(array_keys($diffsAfterCostCompare), 'params'));
    }

    private function compareCost($diffs, $recievedParams) {
        if (!empty($diffs)) {
            foreach ($diffs as $key => $value) {
                if (is_numeric($value) && $this->convertToPriceFormat($this->orderParams[$key]) == $this->convertToPriceFormat($recievedParams[$key])) {
                    unset($diffs[$key]);
                }
            }
        }
        return $diffs;
    }

    public function setWrongRecipientCurrencyTest() {
        $result = $this->Order->setRecipientCurrency('bred');
        $this->showResult(isset($result) && !$result, __FUNCTION__);
    }

    public function setStatusTest() {
        $this->Order->resetParams();

        $resultOfSetCreatedStatus = $this->Order->setStatusCreated();
        $resultOfSetHoldedStatus = $this->Order->setStatusHolded();
        $resultOfSetPartiallyPaidStatus = $this->Order->setStatusPartiallyPaid();
        $resultOfSetPaidStatus = $this->Order->setStatusPaid();
        $resultOfSetRefundedStatus = $this->Order->setStatusRefunded();
        $resultOfSetCancelledStatus = $this->Order->setStatusCancelled();
        $resultOfWrongSetCreatedStatus = $this->Order->setStatusCreated();

        $result = $resultOfSetCreatedStatus && $resultOfSetHoldedStatus && $resultOfSetPartiallyPaidStatus && $resultOfSetPaidStatus && $resultOfSetRefundedStatus && $resultOfSetCancelledStatus && !$resultOfWrongSetCreatedStatus && isset($resultOfWrongSetCreatedStatus);
        $this->showResult($result, __FUNCTION__);
    }

    public function addAndGetItemsTest() {
        $this->Order->resetParams();

        foreach ($this->products as $product) {
            $this->Order->addItem($product['price'], $product['quantity'], $product['name'], 1);
        }

        $result = true;
        foreach ($this->Order->getItems() as $key => $product) {
            if (!($product instanceof Item) || $product->price != $this->products[$key]['price'] || $product->quantity != $this->products[$key]['quantity'] || $product->text != $this->products[$key]['name']) {
                $result = false;
            }
        }

        $this->showResult($result, __FUNCTION__);
    }

}

$ot = OrderTest::getInstance();
$ot->start();
?>

