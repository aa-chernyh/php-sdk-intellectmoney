<?php

namespace PaySystem;

require_once("GlobalTestValues.php");
require_once("../IntellectMoneyCommon/Customer.php");

class CustomerTest extends GlobalTestValues {

    private static $instance;
    private $Customer;

    public static function getInstance() {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function __construct() {
        $this->Customer = Customer::getInstance($this->customerParams['email'], $this->customerParams['name'], $this->customerParams['phone']);
    }

    public function start() {
        $this->getAllParamsTest();
        $this->setAllParamsTest();
        $this->resetParamsTest();
        $this->getContactTest();
    }

    public function getAllParamsTest() {
        $errorFunctions = $this->getErrorFunctions($this->Customer, $this->customerParams);
        $this->showResult(empty($errorFunctions), __FUNCTION__, $this->generateErrorText($errorFunctions));
    }

    public function setAllParamsTest() {
        $this->resetAndSetAllParams($this->Customer, $this->customerParams);
        $errorFunctions = $this->getErrorFunctions($this->Customer, $this->customerParams);
        $this->showResult(empty($errorFunctions), __FUNCTION__, $this->generateErrorText($errorFunctions));
    }

    public function resetParamsTest() {
        $this->resetAndSetAllParams($this->Customer, $this->customerParams);
        $this->Customer->resetParams();

        $result = true;
        foreach ($this->customerParams as $key => $value) {
            $functionName = 'get' . ucfirst($key);
            if ($this->Customer->$functionName() != NULL) {
                $result = false;
            }
        }

        $this->showResult($result, __FUNCTION__);
    }

    public function getContactTest() {
        $this->Customer->resetParams();

        $this->Customer->setPhone($this->customerParams['number']);
        $phone = $this->Customer->getContact();

        $this->Customer->setEmail($this->customerParams['email']);
        $email = $this->Customer->getContact();

        $this->Customer->resetParams();
        $this->Customer->setEmail($this->customerParams['email']);
        $emailAfterResetParams = $this->Customer->getContact();

        $this->showResult($this->customerParams['number'] == $phone && $this->customerParams['email'] == $email && $this->customerParams['email'] == $emailAfterResetParams, __FUNCTION__);
    }

}

$ct = CustomerTest::getInstance();
$ct->start();
?>