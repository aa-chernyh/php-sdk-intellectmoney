<?php

namespace PaySystem;

require_once("../IntellectMoneyCommon/Validator.php");
require_once("GlobalTestValues.php");

class ValidatorTest extends GlobalTestValues {

    private static $instance;
    private $isAllowChangeValue;

    public function getInstance($isAllowChangeValue = false) {
        if (empty(self::$instance)) {
            self::$instance = new self($isAllowChangeValue);
        }
        return self::$instance;
    }

    private function __construct($isAllowChangeValue) {
        $this->isAllowChangeValue = $isAllowChangeValue;
    }

    public function start() {
        echo "<b>Allowed change values:</b> <br>";
        $this->isAllowChangeValue = true;
        $this->allRightValuesTest();
        $this->allWrongValuesTest();
        $this->validateArrayTest();

        echo "<br><b>Disallowed change values:</b> <br>";
        $this->isAllowChangeValue = false;
        $this->allRightValuesTest();
        $this->allWrongValuesTest();
        $this->validateArrayTest();
    }

    public function allRightValuesTest() {
        foreach ($this->rightParams as $name => $value) {
            $this->showResult(Validator::validate($name, $value, false), __FUNCTION__ . ", " . $name);
        }
    }

    public function allWrongValuesTest() {
        foreach ($this->wrongParams as $name => $value) {
            $result;
            if ($this->isAllowChangeValue && in_array($name, $this->paramsContainingDefaultValue)) {
                if ($this->paramsContainingDefaultValue[$name] == $value) {
                    $result = true;
                }
            } else {
                $result = !Validator::validate($name, $value, $this->isAllowChangeValue);
            }
            $this->showResult($result, __FUNCTION__ . ", " . $name);
        }
    }

    public function validateArrayTest() {
        $rightParamsErrors = array();
        Validator::validateArray($this->rightParams, $rightParamsErrors, false);
        $this->showResult(empty($rightParamsErrors), __FUNCTION__ . " right params");

        $wrongParamsErrors = array();
        Validator::validateArray($this->wrongParams, $wrongParamsErrors, $this->isAllowChangeValue);

        if ($this->isAllowChangeValue) {
            $isContentsDefaultValue = false;
            foreach ($this->paramsContainingDefaultValue as $value) {
                if (in_array($value, $wrongParamsErrors)) {
                    $isContentsDefaultValue = true;
                }
            }
            $result = !$isContentsDefaultValue;
        } else {
            $result = true;
            foreach (array_keys($this->rightParams) as $value) {
                if (!in_array($value, $wrongParamsErrors) && $value != 'group') {
                    $result = false;
                }
            }
        }

        $this->showResult($result, __FUNCTION__ . " wrong value");
    }

}

$vt = ValidatorTest::getinstance();
$vt->start();
?>