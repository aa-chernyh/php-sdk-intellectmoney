<?php

namespace PaySystem;

require_once("GlobalTestValues.php");
require_once("../IntellectMoneyCommon/MerchantReceiptHelper.php");
require_once("../IntellectMoneyCommon/Order.php");
require_once("../IntellectMoneyCommon/Customer.php");
require_once("../IntellectMoneyCommon/UserSettings.php");

class MerchantReceiptHelperTest extends GlobalTestValues {

    private static $instance;
    private $MerchantReceiptHelper;
    private $Order;
    private $Customer;
    private $UserSettings;

    public static function getInstance() {
        if (empty(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __construct() {
        $this->Order = Order::getInstance();
        $this->Customer = Customer::getInstance();
        $this->UserSettings = UserSettings::getInstance();
    }

    public function start() {
        $this->generateMerchantReceiptTest();
        $this->generateMerchantReceiptTestWithDifferentValuesAtRecipientAmountAndOriginalAmountWithDiscount();
    }

    public function generateMerchantReceiptTest() {
        $this->setParamsForGenerateMerchantReceipt($this->orderParams, 'phone');
        $this->showResult($this->MerchantReceiptHelper->generateMerchantReceipt(false) == $this->merchantReceipt, __FUNCTION__);
    }

    public function generateMerchantReceiptTestWithDifferentValuesAtRecipientAmountAndOriginalAmountWithDiscount() {
        $this->setParamsForGenerateMerchantReceipt($this->orderParamsWithDifferentValuesAtRecipientAmountAndOriginalAmountWithDiscount, 'email');
        $this->showResult($this->merchantReceiptWithDifferentValuesAtRecipientAmountAndOriginalAmountWithDiscount == $this->MerchantReceiptHelper->generateMerchantReceipt(false), __FUNCTION__);
    }

    private function setParamsForGenerateMerchantReceipt($orderParams, $customerContact) {
        $this->resetAllParams();

        $this->Order->setParams($orderParams['invoiceId'], $orderParams['orderId'], $orderParams['originalAmount'], $orderParams['recipientAmount'], $orderParams['paidAmount'], $orderParams['deliveryAmount'], $orderParams['recipientCurrency'], $orderParams['discount'], $orderParams['status']);
        $this->UserSettings->setParams($this->rightParams);
        $this->Customer->setPhone($this->customerParams[$customerContact]);

        $this->MerchantReceiptHelper = new MerchantReceiptHelper($this->Order->getRecipientAmount(), $this->UserSettings->getInn(), $this->Customer->getContact(), $this->UserSettings->getGroup(), $this->Order->getDiscount(), $this->Order->getOriginalAmount());
        $this->generateItems();
    }

    private function generateItems() {
        foreach ($this->products as $product) {
            $this->MerchantReceiptHelper->addItem($product['price'], $product['quantity'], $product['name'], 1, 2, 3);
        }
        $this->MerchantReceiptHelper->addItem($this->Order->getDeliveryAmount(), 1, "Delivery", 2, 2, 3);
    }

    private function resetAllParams() {
        $this->Customer->resetParams();
        $this->UserSettings->resetParams();
        $this->Order->resetParams();
    }

}

$mrht = MerchantReceiptHelperTest::getInstance();
$mrht->start();
